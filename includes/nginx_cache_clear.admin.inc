<?php

/**
 * @file
 * Settings form for nginx cache clear.
 */

/**
 * Callback from nginx_cache_clear_menu().
 */
function nginx_cache_clear_admin_settings() {
  $form['server_cache_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Server Cache Configuration'),
  );

  $form['server_cache_config']['server_cache_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Webserver cache key'),
    '#required' => TRUE,
    '#default_value' => variable_get('server_cache_key', ''),
    '#description' => t('Please check the server configuration and confirm the hash key used. Key Format : $scheme$request_method$host$request_uri$is_args$args.'),
  );

  $form['server_cache_config']['server_cache_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Webserver cache file path'),
    '#required' => TRUE,
    '#default_value' => variable_get('server_cache_path', ''),
    '#description' => t('Please check the server configuration and enter the path configured to store cache file.'),
  );

  $form['server_cache_config']['server_cache_auto_delete'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto clear cache when content edit'),
    '#required' => FALSE,
    '#default_value' => variable_get('server_cache_auto_delete', ''),
    '#description' => t('If this option is enabled, then the cache file of content(URL alias) will be deleted when the user update the content.'),
  );

  return system_settings_form($form);
}
